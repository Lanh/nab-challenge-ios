//
//  CitySearchingModule.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation

final class CitySearchingModule {
    static func create() -> CityViewProtocol {
        let interactor = CitySearchingInteractor(storage: StorageProvider().makeStorage())
        let presenter = CitySearchingPresenter(interactor: interactor)
        let viewController = CitySearchingViewController(presenter: presenter)
        presenter.view = viewController
        return viewController
    }
}
