//
//  CityModel.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation
import RealmSwift

class CityModel: Object, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var country: String = ""
    
    private enum CodingKeys: String, CodingKey {
        case id = "geonameid"
        case name
        case country
    }
    
    override class func primaryKey() -> String? {
        return Keys.id.rawValue
    }
    
    convenience init(id: Int,
                     name: String,
                     country: String) {
        self.init()
        self.id = id
        self.name = name
        self.country = country
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? CityModel else { return false }
        return id == rhs.id &&
            name == rhs.name &&
            country == rhs.country
    }
    
    enum Keys: String {
        case id
        case name
        case country
    }
}
