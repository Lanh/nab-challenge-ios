//
//  CitySearchHistoryModel.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation
import RealmSwift

class CitySearchHistoryModel: Object {
    @objc dynamic var cityId: Int = 0
    @objc dynamic var createdAt = Date(timeIntervalSince1970: 1)
    
    convenience init(cityId: Int, createdAt: Date) {
        self.init()
        self.cityId = cityId
        self.createdAt = createdAt
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? CitySearchHistoryModel else { return false }
        let dateFormatter = DateFormatter.fullDateFormatter
        return cityId == rhs.cityId
            && dateFormatter.string(from: createdAt) == dateFormatter.string(from: rhs.createdAt)
    }
    
    enum Keys: String {
        case createdAt
    }
}
