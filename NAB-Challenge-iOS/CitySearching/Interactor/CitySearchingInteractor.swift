//
//  CitySearchingInteractor.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation

protocol CitySearchingInteraction {
    func getCities(keyword: String) -> [CityModel]
    func getSearchHistory() -> [CityModel]
    func saveSearchHistory(cityId: Int)
}

class CitySearchingInteractor {
    private let storage: StorageContext
    
    init(storage: StorageContext) {
        self.storage = storage
    }
}

extension CitySearchingInteractor: CitySearchingInteraction {
    func getCities(keyword: String) -> [CityModel] {
        // Find the cities or countries by using the keyword
        // Apply case insensitive and ignore any accented (diacritical) characters
        let sorted = Sorted(key: CityModel.Keys.name.rawValue, ascending: true)
        let predicate = NSPredicate(format: "\(CityModel.Keys.name.rawValue) CONTAINS[cd] %@ OR \(CityModel.Keys.country.rawValue) CONTAINS[cd] %@", keyword, keyword)
        let results = storage.fetch(CityModel.self, predicate: predicate, sorted: sorted)
        return results
    }
    
    func getSearchHistory() -> [CityModel] {
        let sorted = Sorted(key: CitySearchHistoryModel.Keys.createdAt.rawValue, ascending: false)
        let results = storage.fetch(CitySearchHistoryModel.self, sorted: sorted)
        return results.compactMap { storage.fetchById(CityModel.self, id: $0.cityId) }
    }
    
    func saveSearchHistory(cityId: Int) {
        let predicate = NSPredicate(format: "cityId = %d", cityId)
        if let searchHistory = storage.fetch(CitySearchHistoryModel.self, predicate: predicate).first {
            try? storage.update {
                searchHistory.createdAt = Date()
            }
        } else {
            let searchHistory = CitySearchHistoryModel(cityId: cityId, createdAt: Date())
            try? storage.save(object: searchHistory)
        }
    }
}
