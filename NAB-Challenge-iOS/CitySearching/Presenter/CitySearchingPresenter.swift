//
//  CitySearchingPresenter.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation

protocol CitySearchingPresentation {
    func startSearching()
    func searching(keyword: String)
    func finishSearching(with cityId: Int)
}

class CitySearchingPresenter: CitySearchingPresentation {
    private let interactor: CitySearchingInteraction
    private var cities: [CityViewData] = []
    weak var view: CityViewProtocol?
    
    init(interactor: CitySearchingInteraction) {
        self.interactor = interactor
    }
    
    func startSearching() {
        // Retrieve cities in search history
        let searchedCities = interactor.getSearchHistory()
            .map { CityViewData(city: $0, isFromSearchHistory: true) }
        cities = searchedCities
        view?.reload(with: cities)
    }
    
    func searching(keyword: String) {
        // Triggering search by city name
        guard !keyword.isEmpty else {
            startSearching()
            return
        }
        let fetchedCities = interactor.getCities(keyword: keyword)
            .map { CityViewData(city: $0, isFromSearchHistory: false) }
        if fetchedCities.isEmpty {
            view?.displayNoCityFoundState()
        } else {
            cities = fetchedCities
            view?.reload(with: cities)
        }
    }
    
    func finishSearching(with cityId: Int) {
        // Save city into search history
        interactor.saveSearchHistory(cityId: cityId)
    }
}
