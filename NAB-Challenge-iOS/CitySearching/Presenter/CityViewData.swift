//
//  CityViewData.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation

struct CityViewData: Equatable {
    let id: Int
    let name: String
    let country: String
    let isFromSearchHistory: Bool
    
    init(city: CityModel, isFromSearchHistory: Bool) {
        id = city.id
        name = city.name
        country = city.country
        self.isFromSearchHistory = isFromSearchHistory
    }
}
