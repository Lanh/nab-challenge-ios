//
//  CitySearchingViewController.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import UIKit
import EasyPeasy

protocol CityViewProtocol: UIViewController, UISearchBarDelegate {
    var onSelectCity: OnChangeDataAction? { get set }
    func displayNoCityFoundState()
    func reload(with cities: [CityViewData])
}

class CitySearchingViewController: UIViewController, StateDisplay {
    private let presenter: CitySearchingPresentation
    
    var onSelectCity: OnChangeDataAction?
    
    private let tableView = UITableView(frame: .zero, style: .grouped)
    
    private lazy var dataSource: CitySearchingTableViewDataSource = {
        let dataSource = CitySearchingTableViewDataSource(tableView: tableView)
        return dataSource
    }()
    
    private var displayCitiesWorkItem: DispatchWorkItem?
    
    init(presenter: CitySearchingPresentation) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        configureKeyboardNotification()
        handleSelection()
    }
    
    private func setUpViews() {
        view.backgroundColor = .white
        tableView.register(CityCell.self, forCellReuseIdentifier: CityCell.reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        view.addSubview(tableView)
        tableView.easy.layout(Edges())
    }
    
    private func searching(keywords: String) {
        presenter.searching(keyword: keywords)
    }
    
    private func handleSelection() {
        dataSource.didSelect = { [weak self] selectedCity in
            guard let self = self else {
                return
            }
            self.presenter.finishSearching(with: selectedCity.id)
            self.onSelectCity?(selectedCity)
        }
    }
}

private extension CitySearchingViewController {
    func configureKeyboardNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(updateContentInsetForKeyboard(notification:)),
                                       name: UIResponder.keyboardWillHideNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(updateContentInsetForKeyboard(notification:)),
                                       name: UIResponder.keyboardDidChangeFrameNotification,
                                       object: nil)
    }
    
    @objc func updateContentInsetForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else { return }
        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableView.contentInset = .zero
        } else {
            let bottom: CGFloat = keyboardViewEndFrame.height - view.safeAreaInsets.bottom
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
        }
        
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
}

// MARK: - UISearchBarDelegate
extension CitySearchingViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        presenter.startSearching()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            presenter.startSearching()
            return
        }
        
        guard searchText.count > 2 else {
            hideState()
            return
        }
        
        // Cancel the currently work item
        displayCitiesWorkItem?.cancel()
        
        // Create the new displayCitiesWorkItem
        let newWorkItem = DispatchWorkItem { [weak self] in
            self?.searching(keywords: searchText)
        }
        
        // Save the new work item and execute it after 250 ms
        displayCitiesWorkItem = newWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250),
                                      execute: newWorkItem)
    }
}

extension CitySearchingViewController: CityViewProtocol {
    func displayNoCityFoundState() {
        let state = State.error(title: StateText.noCityTitle,
                                message: StateText.noCityMessage)
        displayState(state)
    }
    
    func reload(with cities: [CityViewData]) {
        hideState()
        dataSource.items = cities
    }
}
