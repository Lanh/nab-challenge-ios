//
//  CityCell.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import UIKit
import EasyPeasy

protocol CityCellProtocol {
    static var reuseIdentifier: String { get }
    func setUpData(with viewData: CityViewData)
}

class CityCell: UITableViewCell {
    private enum Defaults {
        static let titleFont = UIFont.preferredFont(forTextStyle: .headline)
        static let subtitleFont = UIFont.preferredFont(forTextStyle: .subheadline)
        static let iconSize: CGFloat = 24.0
    }
    
    private lazy var cityLabel: UILabel = createLabel()
    private lazy var countryLabel: UILabel = createLabel()
    private lazy var iconView: UIImageView = createImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLayout()
        configureDisplay()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension CityCell {
    func configureLayout() {
        contentView.addSubview(cityLabel)
        contentView.addSubview(countryLabel)
        contentView.addSubview(iconView)
        
        iconView.easy.layout(
            Left(CGFloat.regular),
            CenterY(),
            Width(Defaults.iconSize),
            Height(Defaults.iconSize)
        )
        
        cityLabel.easy.layout(
            Top(CGFloat.small),
            Left(CGFloat.regular).to(iconView),
            Right(CGFloat.regular),
            Bottom(CGFloat.xSmall).to(countryLabel)
        )
        
        countryLabel.easy.layout(
            Top(CGFloat.xSmall).to(cityLabel),
            Left(CGFloat.regular).to(iconView),
            Right(CGFloat.regular),
            Bottom(CGFloat.small)
        )
    }
    
    func configureDisplay() {
        cityLabel.font = Defaults.titleFont
        countryLabel.font = Defaults.subtitleFont
        iconView.tintColor = .gray
    }
    
    func createLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        label.adjustsFontForContentSizeCategory = true
        return label
    }
    
    func createImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.adjustsImageSizeForAccessibilityContentSizeCategory = true
        return imageView
    }
    
    func accessibilityContentFor(_ viewData: CityViewData) -> String {
        var content: [String] = []
        
        let iconText = viewData.isFromSearchHistory ? Accessibility.Label.history : Accessibility.Label.result
        content.append(iconText)
        
        let cityText = Accessibility.Label.city + viewData.name
        content.append(cityText)
        
        let countryText = Accessibility.Label.country + viewData.country
        content.append(countryText)
        
        return content.joined(separator: "\n")
    }
}

extension CityCell: CityCellProtocol {
    static var reuseIdentifier: String {
        return String(describing: CityCell.self)
    }
    
    func setUpData(with viewData: CityViewData) {
        cityLabel.attributedText = NSAttributedString(text: viewData.name,
                                                      font: cityLabel.font)
        cityLabel.accessibilityValue = viewData.name
        
        countryLabel.attributedText = NSAttributedString(text: viewData.country,
                                                         font: countryLabel.font)
        countryLabel.accessibilityValue = viewData.country
        
        iconView.image = viewData.isFromSearchHistory ? UIImage(named: ImageName.history) : UIImage(named: ImageName.search)
        
        accessibilityLabel = accessibilityContentFor(viewData)
    }
}
