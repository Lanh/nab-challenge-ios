//
//  AppDelegate.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        loadCityListIfNeeded()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(
            rootViewController: WeatherForecastModule.create()
        )
        window?.makeKeyAndVisible()
        return true
    }
    
    private func loadCityListIfNeeded() {
        // Only load the list of city once
        let settings = UserDefaultSettings()
        if !settings.loadBool(key: .cityList) {
            // Insert the city list into local storage
            if let cities: [CityModel] = JSONParser().parse(name: "world-cities") {
                let realmStorage = StorageProvider().makeStorage()
                try? realmStorage.save(objects: cities)
                settings.saveBool(key: .cityList, value: true)
            }
        }
    }
}

