//
//  WeatherForecastPresenter.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation

protocol WeatherForecastPresentation {
    func didStart()
}

final class WeatherForecastPresenter {
    private let interactor: WeatherForecastInteraction
    private let router: WeatherForecastRoutes
    private var viewData: [DailyForecastViewData] = []
    
    weak var view: WeatherForecastViewProtocol?
    
    init(interactor: WeatherForecastInteraction, router: WeatherForecastRoutes) {
        self.interactor = interactor
        self.router = router
    }
}

extension WeatherForecastPresenter: WeatherForecastPresentation {
    func didStart() {
        router.embedCitySearching { [unowned self] selectedCity in
            self.view?.updateSearchBar(with: selectedCity.name)
            self.didSelectCity(with: selectedCity.id)
        }
    }
    
    private func didSelectCity(with cityId: Int) {
        view?.displayLoadingState()
        fetchWeatherForecast(by: cityId)
    }
    
    private func fetchWeatherForecast(by cityId: Int) {
        // Check if there is a valid cached, then returns it
        let (isValid, cachedResults) = interactor.hasValidCachedWeatherForecast(by: cityId)
        if isValid,
           cachedResults.count > 0 {
            self.updateData(cachedResults)
            return
        }
        
        interactor.fetchWeatherForecast(by: cityId, isMocked: false) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(models):
                self.updateData(models)
            case let .failure(error):
                if let networkError = error as? NetworkError {
                    self.view?.displayErrorState(errorMessage: networkError.errorDescription)
                } else {
                    self.view?.displayErrorState(errorMessage: ErrorText.general)
                }
            }
        }
    }
    
    private func updateData(_ models: [DailyForecastModel]) {
        // Transfer the model into view data
        viewData = models.map({
            DailyForecastViewData(model: $0, tempUnit: DefaultUnits.tempUnit.rawValue)
        })
        view?.reload(with: viewData)
    }
}
