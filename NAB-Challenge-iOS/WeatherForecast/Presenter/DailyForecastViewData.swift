//
//  DailyForecastViewModel.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation

private enum Localized {
    static let prefixDate = "Date: "
    static let prefixTemp = "Average Temperature: "
    static let prefixPressure = "Pressure: "
    static let prefixHumidity = "Humidity: "
    static let prefixDescn = "Description: "
}

struct DailyForecastViewData: Equatable {
    let date: String
    let averageTemp: String
    let pressure: String
    let humidity: String
    let descn: String
}

extension DailyForecastViewData {
    init(model: DailyForecastModel, tempUnit: String) {
        date = Localized.prefixDate + DateFormatter.unixDateFormatter.string(from: model.date)
        var symbol = ""
        if let unit = TempUnit.allCases.first(where: { $0.rawValue == tempUnit }) {
            symbol = unit.shortTextDisplay
        }
        averageTemp = Localized.prefixTemp + model.averageTemp + symbol
        pressure = Localized.prefixPressure + model.pressure
        humidity = Localized.prefixHumidity + model.humidity + "%"
        descn = Localized.prefixDescn + model.descn
    }
}
