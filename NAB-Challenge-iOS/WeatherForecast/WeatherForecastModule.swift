//
//  WeatherForecastModule.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation

final class WeatherForecastModule {
    static func create() -> WeatherForecastViewProtocol {
        let interactor = WeatherForecastInteractor(connectivity: Connectivity(),
                                                   storage: StorageProvider().makeStorage())
        let router = WeatherForecastRouter()
        let presenter = WeatherForecastPresenter(interactor: interactor, router: router)
        let viewController = WeatherForecastViewController(presenter: presenter)
        presenter.view = viewController
        router.viewController = viewController
        return viewController
    }
}
