//
//  DailyForecastModel.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation
import RealmSwift

final class DailyForecastModel: Object {
    @objc dynamic var date = Date(timeIntervalSince1970: 1)
    @objc dynamic var averageTemp: String = ""
    @objc dynamic var pressure: String = ""
    @objc dynamic var humidity: String = ""
    @objc dynamic var descn: String = ""
    
    convenience init(response: WeatherForecastResponseDaily) {
        self.init()
        date = Date(timeIntervalSince1970: response.timestamp)
        averageTemp = String(format: "%.0f", (response.temp.min + response.temp.max) / 2.0)
        humidity = String(format: "%.0f", response.humidity)
        pressure = String(format: "%.0f", response.pressure)
        descn = response.weather.first?.descn ?? ""
    }
    
    convenience init(date: Date,
                     averageTemp: String,
                     pressure: String,
                     humidity: String,
                     descn: String) {
        self.init()
        self.date = date
        self.averageTemp = averageTemp
        self.pressure = pressure
        self.humidity = humidity
        self.descn = descn
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? DailyForecastModel else { return false }
        let dateFormatter = DateFormatter.fullDateFormatter
        return dateFormatter.string(from: date) == dateFormatter.string(from: rhs.date)
            && averageTemp == rhs.averageTemp
            && pressure == rhs.pressure
            && humidity == rhs.humidity
            && descn == rhs.descn
    }
}
