//
//  WeatherForecastResponse.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation

struct WeatherForecastResponse: Decodable {
    let list: [WeatherForecastResponseDaily]
}

struct WeatherForecastResponseDaily: Decodable {
    let timestamp: Double
    let pressure: Float
    let humidity: Float
    let temp: Temperature
    let weather: [Weather]
    
    private enum CodingKeys: String, CodingKey {
        case timestamp = "dt"
        case pressure = "pressure"
        case humidity = "humidity"
        case temp = "temp"
        case weather = "weather"
    }
    
    struct Temperature: Decodable {
        let min: Float
        let max: Float
    }
    
    struct Weather: Decodable {
        let descn: String
        
        private enum CodingKeys: String, CodingKey {
            case descn = "description"
        }
    }
}
