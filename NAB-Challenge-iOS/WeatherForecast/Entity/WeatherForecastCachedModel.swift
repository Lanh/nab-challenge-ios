//
//  WeatherForecastCachedModel.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation
import RealmSwift

final class WeatherForecastCachedModel: Object {
    @objc dynamic var cityId: Int = 0
    @objc dynamic var days: Int = 0
    @objc dynamic var tempUnit: String = ""
    @objc dynamic var createdAt = Date(timeIntervalSince1970: 1)
    let forecastData = List<DailyForecastModel>()
    
    convenience init(cityId: Int,
                     days: Int,
                     tempUnit: String,
                     createAt: Date,
                     forecastData: [DailyForecastModel]) {
        self.init()
        self.cityId = cityId
        self.days = days
        self.tempUnit = tempUnit
        self.createdAt = createAt
        self.forecastData.removeAll()
        forecastData.forEach { self.forecastData.append($0) }
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? WeatherForecastCachedModel else { return false }
        let dateFormatter = DateFormatter.fullDateFormatter
        return cityId == rhs.cityId
            && days == rhs.days
            && tempUnit == rhs.tempUnit
            && dateFormatter.string(from: createdAt) == dateFormatter.string(from: rhs.createdAt)
            && forecastData == rhs.forecastData
    }
    
    enum Keys: String {
        case cityId
        case days
        case tempUnit
        case createdAt
    }
}
