//
//  WeatherForecastRouter.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import UIKit

typealias OnChangeDataAction = (CityViewData) -> Void

protocol WeatherForecastRoutes {
    func embedCitySearching(_ onChangeDataAction: OnChangeDataAction?)
}

final class WeatherForecastRouter: WeatherForecastRoutes {
    weak var viewController: WeatherForecastViewController?
    
    func embedCitySearching(_ onChangeDataAction: OnChangeDataAction?) {
        let searchResultsController = CitySearchingModule.create()
        searchResultsController.onSelectCity = onChangeDataAction
        let searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.searchBar.delegate = searchResultsController
        viewController?.navigationItem.searchController = searchController
    }
}
