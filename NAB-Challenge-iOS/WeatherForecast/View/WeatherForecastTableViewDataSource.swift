//
//  WeatherForecastTableViewDataSource.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 08/09/2021.
//

import UIKit

final class WeatherForecastTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    private weak var tableView: UITableView?
    
    var items: [DailyForecastViewData]? {
        didSet { reload() }
    }
    
    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
    }
    
    private func reload() {
        tableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: WeatherForecastCell.reuseIdentifier,
                                                    for: indexPath) as? WeatherForecastCell {
            let index = indexPath.row
            if let items = items,
               index < items.count {
                cell.setUpData(with: items[index])
            }
            return cell
        }
        fatalError("Not found cell!")
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
}
