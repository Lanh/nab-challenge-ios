//
//  WeatherForecastViewController.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import UIKit
import EasyPeasy

protocol WeatherForecastViewProtocol: UIViewController {
    func displayLoadingState()
    func displayErrorState(errorMessage: String)
    func reload(with viewData: [DailyForecastViewData])
    func updateSearchBar(with searchText: String)
}

final class WeatherForecastViewController: UIViewController {
    private let presenter: WeatherForecastPresentation
    
    private let tableView = UITableView(frame: .zero, style: .grouped)
    
    private lazy var dataSource: WeatherForecastTableViewDataSource = {
        let dataSource = WeatherForecastTableViewDataSource(tableView: tableView)
        return dataSource
    }()
    
    init(presenter: WeatherForecastPresentation) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.didStart()
        setUpViews()
        setUpSearchBar()
    }
    
    private func setUpViews() {
        view.backgroundColor = .white
        navigationItem.title = NavigationTitle.weather
        tableView.register(WeatherForecastCell.self, forCellReuseIdentifier: WeatherForecastCell.reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        view.addSubview(tableView)
        tableView.easy.layout(Edges())
    }
    
    private func setUpSearchBar() {
        if #available(iOS 13.0, *) {
            navigationItem.searchController?.showsSearchResultsController = true
        } else {
            navigationItem.searchController?.searchResultsUpdater = self
        }
        navigationItem.searchController?.obscuresBackgroundDuringPresentation = true
        navigationItem.searchController?.searchBar.placeholder = Text.searchPlaceholder
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
}


extension WeatherForecastViewController: WeatherForecastViewProtocol, StateDisplay {
    func displayLoadingState() {
        let state = State.loading(message: StateText.loading)
        displayState(state)
    }
    
    func displayErrorState(errorMessage: String) {
        let state = State.error(title: StateText.errorTitle, message: errorMessage)
        displayState(state)
    }
    
    func reload(with viewData: [DailyForecastViewData]) {
        hideState()
        dataSource.items = viewData
    }
    
    func updateSearchBar(with searchText: String) {
        navigationItem.searchController?.isActive = false
        navigationItem.searchController?.searchBar.text = searchText
    }
}

// MARK: - UISearchResultsUpdating
extension WeatherForecastViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if #available(*, iOS 12.0) { // Always show searchResultsController
            searchController.searchResultsController?.view.isHidden = false
        }
    }
}
