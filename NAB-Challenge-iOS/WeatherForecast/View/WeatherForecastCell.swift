//
//  WeatherForecastCell.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import UIKit
import EasyPeasy

protocol WeatherForecastCellProtocol {
    static var reuseIdentifier: String { get }
    func setUpData(with viewData: DailyForecastViewData)
}

final class WeatherForecastCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var contentLabel: UILabel = createLabel()
    
    private func setUpViews() {
        selectionStyle = .none
        contentLabel.font = UIFont.preferredFont(forTextStyle: .body)
        contentView.addSubview(contentLabel)
        contentLabel.easy.layout(Edges(CGFloat.regular))
    }
    
    private func createLabel() -> UILabel {
        let label = UILabel()
        label.adjustsFontForContentSizeCategory = true
        label.isAccessibilityElement = true
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        return label
    }
    
    private func accessibilityContentFor(_ viewModel: DailyForecastViewData) -> String {
        var content: [String] = []
        let mirror = Mirror(reflecting: viewModel)
        
        for child in mirror.children {
            if let value = child.value as? String {
                content.append(value)
            }
        }
        
        return content.joined(separator: "\n")
    }
}

extension WeatherForecastCell: WeatherForecastCellProtocol {
    static var reuseIdentifier: String {
        return String(describing: WeatherForecastCell.self)
    }
    
    func setUpData(with viewData: DailyForecastViewData) {
        let content = accessibilityContentFor(viewData)
        contentLabel.attributedText = NSAttributedString(text: content,
                                                         font: contentLabel.font)
        accessibilityLabel = content
    }
}
