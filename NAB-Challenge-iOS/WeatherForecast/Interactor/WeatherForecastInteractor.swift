//
//  WeatherForecastInteractor.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation
import Moya

protocol WeatherForecastInteraction {
    func fetchWeatherForecast(by cityId: Int,
                              isMocked: Bool,
                              completion: @escaping (Result<[DailyForecastModel], Error>) -> Void)
    func hasValidCachedWeatherForecast(by cityId: Int) -> (Bool, [DailyForecastModel])
}

final class WeatherForecastInteractor: WeatherForecastInteraction {
    private let connectivity: Reachability
    private let storage: StorageContext
    
    init(connectivity: Reachability,
         storage: StorageContext) {
        self.connectivity = connectivity
        self.storage = storage
    }
    
    func fetchWeatherForecast(by cityId: Int,
                              isMocked: Bool,
                              completion: @escaping (Result<[DailyForecastModel], Error>) -> Void) {
        let parameters = createParameters(by: cityId)
        // Check if there is an available internet connection first
        if !connectivity.isConnectedToInternet {
            completion(.failure(NetworkError.noConnection))
            return
        }
        let target = OpenWeatherAPIConfig.daily(parameters)
        Network.request(target: target) { [weak self] (result: Result<WeatherForecastResponse, Error>) in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                let daysForecast = response.list.map { DailyForecastModel(response: $0) }
                
                // Save the data into database
                self.saveWeatherForecastIntoLocalStorage(parameters: parameters,
                                                         forecastData: daysForecast)
                
                completion(.success(daysForecast))
            case let .failure(error):
                completion(.failure(error))
            }
        } stub: { _ -> StubBehavior in
            guard !isMocked else {
                return .immediate
            }
            return .never
        }
    }
    
    func hasValidCachedWeatherForecast(by cityId: Int) -> (Bool, [DailyForecastModel]) {
        let parameters = createParameters(by: cityId)
        if let cachedData = getWeatherForecastFromLocalStorage(parameters: parameters) {
            let minutes = Calendar.current.dateComponents([.minute],
                                                          from: cachedData.createdAt,
                                                          to: Date()).minute ?? 0
            if minutes < DefaultUnits.livingTime {
                return (true, Array(cachedData.forecastData))
            }
        }
        return (false, [])
    }
    
    private func getWeatherForecastFromLocalStorage(parameters: OpenWeatherAPIConfig.Parameters) -> WeatherForecastCachedModel? {
        let cityId = WeatherForecastCachedModel.Keys.cityId.rawValue
        let days = WeatherForecastCachedModel.Keys.days.rawValue
        let tempUnit = WeatherForecastCachedModel.Keys.tempUnit.rawValue
        
        let predicate = NSPredicate(format: "\(cityId) = %d && \(days) = %d && \(tempUnit) = %@",
                                    parameters.cityId,
                                    parameters.days,
                                    parameters.tempUnit)
        
        return storage.fetch(WeatherForecastCachedModel.self, predicate: predicate).first
    }
    
    private func saveWeatherForecastIntoLocalStorage(parameters: OpenWeatherAPIConfig.Parameters,
                                                     forecastData: [DailyForecastModel]) {
        if let savedData = getWeatherForecastFromLocalStorage(parameters: parameters) {
            try? storage.update {
                savedData.createdAt = Date()
                savedData.forecastData.removeAll()
                forecastData.forEach { savedData.forecastData.append($0) }
            }
        } else {
            let weatherForecast = WeatherForecastCachedModel(cityId: parameters.cityId,
                                                             days: parameters.days,
                                                             tempUnit: parameters.tempUnit,
                                                             createAt: Date(),
                                                             forecastData: forecastData)
            try? storage.save(object: weatherForecast)
        }
    }
    
    private func createParameters(by cityId: Int) -> OpenWeatherAPIConfig.Parameters {
        return OpenWeatherAPIConfig.Parameters(cityId: cityId,
                                               days: DefaultUnits.days,
                                               tempUnit: DefaultUnits.tempUnit.rawValue)
    }
}
