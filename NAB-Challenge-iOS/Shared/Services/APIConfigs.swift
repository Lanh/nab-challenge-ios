//
//  APIConfigs.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation
import Moya

private enum Defaults {
    static let APIKey = "60c6fbeb4b93ac653c492ba806fc346d"
}

enum OpenWeatherAPIConfig {
    case daily(_ parameters: Parameters)
    
    struct Parameters {
        let cityId: Int
        let days: Int
        let tempUnit: String
    }
}

extension OpenWeatherAPIConfig: TargetType {
    var baseURL: URL {
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/") else {
            fatalError("URL is not valid!")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .daily:
            return "forecast/daily"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .daily:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .daily(let parameters):
            let parameters: [String: Any] = [
                "cnt": parameters.days,
                "appid": Defaults.APIKey,
                "units": parameters.tempUnit,
                "id": parameters.cityId
            ]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .daily:
            let data = JSONParser().readLocalFile(name: "weather_forecast_mock")
            return data ?? Data()
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
}
