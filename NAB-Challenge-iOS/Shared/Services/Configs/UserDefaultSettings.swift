//
//  UserDefaultSettings.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 07/09/2021.
//

import Foundation

enum SettingKeys: String {
    case cityList
}

protocol BoolSettingsStoring {
    func saveBool(key: SettingKeys, value: Bool)
    func loadBool(key: SettingKeys) -> Bool
    func remove(key: SettingKeys)
}

class UserDefaultSettings {
    private lazy var defaults: UserDefaults = {
        return UserDefaults.standard
    }()
}

extension UserDefaultSettings: BoolSettingsStoring {
    func loadBool(key: SettingKeys) -> Bool {
        return defaults.bool(forKey: key.rawValue)
    }
    
    func saveBool(key: SettingKeys, value: Bool) {
        defaults.set(value, forKey: key.rawValue)
    }
    
    func remove(key: SettingKeys) {
        defaults.removeObject(forKey: key.rawValue)
    }
}
