//
//  StorageContext.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation
import RealmSwift

struct Sorted {
    var key: String
    var ascending: Bool = true
}

public protocol Storable { }

extension Object: Storable { }

/*
 Operations on context
 */
protocol StorageContext {
    /*
     Save an object conformed to the `Storable` protocol
     */
    func save(object: Storable) throws
    
    /*
     Save array of objects conformed to the `Storable` protocol
     */
    func save(objects: [Storable]) throws
    
    /*
     Update an object conformed to the `Storable` protocol
     */
    func update(block: @escaping () -> Void) throws
    
    /*
     Delete an object conformed to the `Storable` protocol
     */
    func delete(object: Storable) throws
    
    /*
     Delete array of objects conformed to the `Storable` protocol
     */
    func delete(objects: [Storable]) throws
    
    /*
     Delete all objects conformed to the `Storable` protocol
     */
    func deleteAll<T: Storable>(_ model: T.Type) throws
    
    /*
     Return a list of objects conformed to the `Storable` protocol
     */
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate?, sorted: Sorted?) -> [T]
    
    /*
     Return an object with the given id (integer). The primary key is get automatically
    */
    func fetchById<T: Storable>(_ model: T.Type, id: Int) -> T?
    
    /*
     Return an object with the given id (string). The primary key is get automatically
    */
    func fetchBySid<T: Storable>(_ model: T.Type, sid: String) -> T?
}

extension StorageContext {
    /*
     Return a list of objects that are conformed to the `Storable` protocol with some default values
     https://medium.com/@georgetsifrikas/swift-protocols-with-default-values-b7278d3eef22
     */
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate? = nil, sorted: Sorted? = nil) -> [T] {
        return fetch(model, predicate: predicate, sorted: sorted)
    }
}
