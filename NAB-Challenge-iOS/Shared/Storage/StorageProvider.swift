//
//  StorageProvider.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation

class StorageProvider {
    func makeStorage() -> StorageContext {
        return RealmStorageContext()
    }
}
