//
//  RealmStorageContext.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation
import RealmSwift

private let CURRENT_SCHEMA_VERSION: UInt64 = 1

class RealmStorageContext: StorageContext {
    static let configuration = Realm.Configuration(schemaVersion: CURRENT_SCHEMA_VERSION)
    
    private(set) var realm: Realm
    
    required init() {
        do {
            try self.realm = Realm(configuration: RealmStorageContext.configuration)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    init(configuration: Realm.Configuration) {
        do {
            try self.realm = Realm(configuration: configuration)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func safeWrite(_ block: (() throws -> Void)) throws {
        if realm.isInWriteTransaction {
            try block()
        } else {
            try realm.write(block)
        }
    }
    
    func reset() throws {
        try self.safeWrite {
            realm.deleteAll()
        }
    }
}

extension RealmStorageContext {
    /*
     Save an object conformed to the `Storable` protocol
     */
    func save(object: Storable) throws {
        guard let object = object as? Object else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        try self.safeWrite {
            if type(of: object).primaryKey() != nil {
                realm.add(object, update: .modified)
            } else {
                realm.add(object, update: .error)
            }
        }
    }
    
    /*
     Save array of objects conformed to the `Storable` protocol
     */
    func save(objects: [Storable]) throws {
        let objects = objects.compactMap({ $0 as? Object })
        try self.safeWrite {
            for object in objects {
                if type(of: object).primaryKey() != nil {
                    realm.add(object, update: .modified)
                } else {
                    realm.add(object, update: .error)
                }
            }
        }
    }
    
    /*
     Update an object conformed to the `Storable` protocol
     */
    func update(block: @escaping () -> Void) throws {
        try self.safeWrite {
            block()
        }
    }

    /*
     Delete an object conformed to the `Storable` protocol
     */
    func delete(object: Storable) throws {
        guard let object = object as? Object else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        try self.safeWrite {
            realm.delete(object)
        }
    }
    
    /*
     Delete array of objects conformed to the `Storable` protocol
     */
    func delete(objects: [Storable]) throws {
        let array = objects.compactMap({ $0 as? Object })
        try self.safeWrite {
            realm.delete(array)
        }
    }
    
    /*
     Delete all objects conformed to the `Storable` protocol
     */
    func deleteAll<T: Storable>(_ model: T.Type) throws {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        try self.safeWrite {
            let objects = realm.objects(type)
            
            for object in objects {
                realm.delete(object)
            }
        }
    }

    /*
     Return a list of objects conformed to the `Storable` protocol
     */
    func fetch<T: Storable>(_ model: T.Type, predicate: NSPredicate? = nil, sorted: Sorted? = nil) -> [T] {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        var objects = self.realm.objects(type)
        
        if let predicate = predicate {
            objects = objects.filter(predicate)
        }
        
        if let sorted = sorted {
            objects = objects.sorted(byKeyPath: sorted.key, ascending: sorted.ascending)
        }
        
        var accumulate: [T] = [T]()
        for object in objects {
            if let object = object as? T {
                accumulate.append(object)
            }
        }
        
        return accumulate
    }
    
    /*
     Return an object with the given id (integer). The primary key is get automatically
    */
    func fetchById<T: Storable>(_ model: T.Type, id: Int) -> T? {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        if let primaryKey = type.primaryKey() {
            let predicate = NSPredicate(format: "\(primaryKey) = %d", id)
            return fetch(type, predicate: predicate, sorted: nil).first as? T
        }
        return nil
    }
    
    /*
     Return an object with the given id (string). The primary key is get automatically
    */
    func fetchBySid<T: Storable>(_ model: T.Type, sid: String) -> T? {
        guard let type = model as? Object.Type else {
            fatalError("REALM ERROR: The model should be an Object type")
        }
        
        if let primaryKey = type.primaryKey() {
            let predicate = NSPredicate(format: "\(primaryKey) = %@", sid)
            return fetch(type, predicate: predicate, sorted: nil).first as? T
        }
        return nil
    }
}
