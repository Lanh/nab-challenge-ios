//
//  Utilities.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation
import UIKit

extension DateFormatter {
    static let unixDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let abbreviation = TimeZone.current.abbreviation() ?? "UTC"
        formatter.timeZone = TimeZone(abbreviation: abbreviation)
        formatter.locale = Locale.current
        formatter.dateFormat = "E, d MMM yyyy"
        return formatter
    }()
    
    static let fullDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let abbreviation = TimeZone.current.abbreviation() ?? "UTC"
        formatter.timeZone = TimeZone(abbreviation: abbreviation)
        formatter.locale = Locale.current
        formatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        return formatter
    }()
}

extension CGFloat {
    static let xSmall: CGFloat = 4.0
    static let tiny: CGFloat = 8.0
    static let small: CGFloat = 12.0
    static let regular: CGFloat = 16.0
}

extension NSAttributedString {
    convenience init(text: String,
                     font: UIFont,
                     lineSpacing: CGFloat = 2.0,
                     textAlignment: NSTextAlignment = .natural) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.hyphenationFactor = 1.0
        
        let attributedString = NSMutableAttributedString(string: text)
        let range = NSRange(location: 0, length: attributedString.length)
        attributedString.addAttribute(.font, value: font, range: range)
        attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: range)
        self.init(attributedString: attributedString)
    }
}

extension UIViewController {
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
}

extension String {
    func dropDiacritic() -> String {
        return self.folding(options: .diacriticInsensitive, locale: .current)
    }
}
