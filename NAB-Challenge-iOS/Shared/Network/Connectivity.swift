//
//  Connectivity.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Alamofire

protocol Reachability {
    var isConnectedToInternet: Bool { get }
}

class Connectivity {
    private let reachabilityManager: NetworkReachabilityManager?
    
    init() {
        self.reachabilityManager = NetworkReachabilityManager()
    }
}

extension Connectivity: Reachability {
    var isConnectedToInternet: Bool {
        return reachabilityManager?.isReachable == true
    }
}
