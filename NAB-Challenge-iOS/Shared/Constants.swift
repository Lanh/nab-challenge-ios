//
//  Constants.swift
//  NAB-Challenge-iOS
//
//  Created by Lanh Vo on 06/09/2021.
//

import Foundation

enum NavigationTitle {
    static let weather = "Weather Forecast"
}

enum ErrorText {
    static let general = "Something went wrong.\nPlease try again later!"
}

enum Text {
    static let searchPlaceholder = "Type the name of city or country"
}

enum ImageName {
    static let info = "information"
    static let error = "error"
    static let search = "search"
    static let history = "history"
}

enum StateText {
    static let noCityTitle = "No city found"
    static let noCityMessage = "There is no result for the above selection. Please try with another one."
    static let loading = "Loading data..."
    static let errorTitle = "An error happened"
}

enum DefaultUnits {
    static let days: Int = 7
    static let tempUnit: TempUnit = .metric
    static let livingTime: Int = 5 // 5 minutes
}

enum TempUnit: String, CaseIterable {
    case standard
    case metric
    case imperial
    
    var longTextDisplay: String {
        switch self {
        case .standard: return "Kelvin"
        case .metric: return "Celsius"
        case .imperial: return "Fahrenheit"
        }
    }
    
    var shortTextDisplay: String {
        switch self {
        case .standard: return UnitTemperature.kelvin.symbol
        case .metric: return UnitTemperature.celsius.symbol
        case .imperial: return UnitTemperature.fahrenheit.symbol
        }
    }
}

enum Accessibility {
    enum Label {
        static let city = "City"
        static let country = "Country"
        static let history = "History"
        static let result = "Result"
    }
}
