platform :ios, '12.0'

install! 'cocoapods',
# Generate a different .xcproj file for each pod, which in itself doesn't mean much but affect the below option
:generate_multiple_pod_projects => false,

# greatly improve install speed if not many pods are changed. Can only be used if pod is separated in its own .xcodeproj
:incremental_installation => false,

# Allow you to modify pod source code, which is undesirable, but greatly improve pod install speed which is sweet for CI.
:lock_pod_sources => false,

# Disable input output path for cocoapods copy script phase
:disable_input_output_paths => true,

:warn_for_unused_master_specs_repo => false

def share_pods
  pod 'Alamofire', '~> 5.4.3'
  pod 'Moya', '~> 15.0.0'
  pod 'RealmSwift', '~> 10.14.0'
  pod 'EasyPeasy', '~> 1.10.0'
end

target 'NAB-Challenge-iOS' do
  # Comment the next line if you don't want to use dynamic frameworks
  use_frameworks!

  # Pods for iOS-NAB
  share_pods

  target 'NAB-Challenge-iOSTests' do
    inherit! :search_paths
    # Pods for testing
  end
end

target 'NAB-Challenge-iOSUITests' do
  # Pods for testing
end

post_install do |installer|
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
      if config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'].to_f < 12.0
        config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '12.0'
      end
    end
  end
end
