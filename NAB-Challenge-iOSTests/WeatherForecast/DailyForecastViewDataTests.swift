//
//  DailyForecastViewDataTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 09/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

final class DailyForecastViewDataTests: XCTestCase {
    private lazy var dailyForecastModels = getDailyForecastModels()
    
    override func setUpWithError() throws {}
    
    override func tearDownWithError() throws {}
    
    func test_dailyForecastViewData_formatter() {
        let viewDataFromModel = DailyForecastViewData(model: dailyForecastModels[0], tempUnit: TempUnit.metric.rawValue)
        let viewData = DailyForecastViewData(date: "Date: Thu, 9 Sep 2021",
                                             averageTemp: "Average Temperature: 27°C",
                                             pressure: "Pressure: 1009",
                                             humidity: "Humidity: 65%",
                                             descn: "Description: moderate rain")
        XCTAssertEqual(viewData, viewDataFromModel)
    }
    
    func test_dailyForecastViewData_withTheSameModelAndUnit() {
        let firstViewData = DailyForecastViewData(model: dailyForecastModels[0], tempUnit: TempUnit.metric.rawValue)
        let secondViewData = DailyForecastViewData(model: dailyForecastModels[0], tempUnit: TempUnit.metric.rawValue)
        XCTAssertEqual(firstViewData, secondViewData)
    }
    
    func test_dailyForecastViewData_withTheSameModelAndDifferentUnit() {
        let firstViewData = DailyForecastViewData(model: dailyForecastModels[0], tempUnit: TempUnit.standard.rawValue)
        let secondViewData = DailyForecastViewData(model: dailyForecastModels[0], tempUnit: TempUnit.metric.rawValue)
        XCTAssertNotEqual(firstViewData, secondViewData)
    }
    func test_dailyForecastViewData_withDifferentModel() {
        let firstViewData = DailyForecastViewData(model: dailyForecastModels[0], tempUnit: TempUnit.metric.rawValue)
        let secondViewData = DailyForecastViewData(model: dailyForecastModels[1], tempUnit: TempUnit.metric.rawValue)
        XCTAssertNotEqual(firstViewData, secondViewData)
    }
}

private extension DailyForecastViewDataTests {
    func getDailyForecastModels() -> [DailyForecastModel] {
        let testBundle = Bundle(for: type(of: self))
        let fileName = "weather_forecast_mock"
        let listingJsonPath = testBundle.url(forResource: fileName, withExtension: "json")!
        let jsonData = try! Data(contentsOf: listingJsonPath)
        
        let data = try! JSONDecoder().decode(WeatherForecastResponse.self, from: jsonData)
        return data.list.map { DailyForecastModel(response: $0) }
    }
}
