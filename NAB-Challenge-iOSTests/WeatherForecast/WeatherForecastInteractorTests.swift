//
//  WeatherForecastInteractorTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 09/09/2021.
//

import XCTest
import RealmSwift
@testable import NAB_Challenge_iOS

private enum Defaults {
    static let cityId = 1580578
    static let days = 7
    static let tempUnit = TempUnit.metric.rawValue
}

final class WeatherForecastInteractorTests: XCTestCase {
    private var sut: WeatherForecastInteractor!
    
    private let connectivity = ConnectivityMock()
    
    private lazy var realmStorage: RealmStorageContext = {
        let configuration = Realm.Configuration(inMemoryIdentifier: "WeatherForecastInteractorTests")
        return RealmStorageContext(configuration: configuration)
    }()
    
    override func setUpWithError() throws {
        sut = WeatherForecastInteractor(connectivity: connectivity,
                                        storage: realmStorage)
    }
    
    override func tearDownWithError() throws {
        try realmStorage.reset()
        connectivity.reset()
    }
    
    func test_hasValidCachedWeatherForecast_withNoCached() {
        // When
        let (isValid, cachedResults) = sut.hasValidCachedWeatherForecast(by: Defaults.cityId)
        
        // Then
        XCTAssertEqual(isValid, false)
        XCTAssertEqual(cachedResults, [])
    }
    
    func test_hasValidCachedWeatherForecast_withValidCached() throws {
        // Given
        let timeInterval = Double(DefaultUnits.livingTime * 60) * 0.9
        let createdAt = Date().addingTimeInterval(-timeInterval)
        try createTestData(createdAt: createdAt)
        
        // When
        let (isValid, cachedResults) = sut.hasValidCachedWeatherForecast(by: Defaults.cityId)
        
        // Then
        XCTAssertEqual(isValid, true)
        XCTAssertEqual(cachedResults, getDailyForecastModels())
    }
    
    func test_hasValidCachedWeatherForecast_withInValidCached() throws {
        // Given
        let timeInterval = Double(DefaultUnits.livingTime * 60) * 1.1
        let createdAt = Date().addingTimeInterval(-timeInterval)
        try createTestData(createdAt: createdAt)
        
        // When
        let (isValid, cachedResults) = sut.hasValidCachedWeatherForecast(by: Defaults.cityId)
        
        // Then
        XCTAssertEqual(isValid, false)
        XCTAssertEqual(cachedResults, [])
    }
    
    func test_fetchWeatherForecast_withNoInternetConnection() {
        // Given
        connectivity.isConnectedToInternet = false
        
        // When
        sut.fetchWeatherForecast(by: Defaults.cityId, isMocked: true, completion: { result in
            switch result {
            case .success:
                XCTFail("Should not fall into this case!")
                
            case let .failure(error):
                // Then
                XCTAssertEqual(error as? NetworkError, .noConnection)
            }
        })
    }
    
    func test_fetchWeatherForecast_withMockedRequestAndNoCached() {
        // Given
        connectivity.isConnectedToInternet = true
        let expectedModels = getDailyForecastModels()
        
        // When
        sut.fetchWeatherForecast(by: Defaults.cityId, isMocked: true) { [unowned self] result in
            switch result {
            case let .success(models):
                // Then
                XCTAssertEqual(models, expectedModels)
                
                let savedData = self.sut.hasValidCachedWeatherForecast(by: Defaults.cityId).1
                XCTAssertEqual(savedData, expectedModels)
            case .failure:
                XCTFail("Should not fall into this case!")
            }
        }
    }
    
    func test_fetchWeatherForecast_withMockedRequestAndInvalidCached() throws {
        // Given
        connectivity.isConnectedToInternet = true
        let expectedModels = getDailyForecastModels()
        let expectedCreatedAt = Date()
        let timeInterval = Double(DefaultUnits.livingTime * 60) * 1.1
        try createTestData(createdAt: expectedCreatedAt.addingTimeInterval(-timeInterval))
        
        // When
        sut.fetchWeatherForecast(by: Defaults.cityId, isMocked: true) { [unowned self] result in
            switch result {
            case let .success(models):
                // Then
                XCTAssertEqual(models, expectedModels)
                
                let latest = self.realmStorage.fetch(WeatherForecastCachedModel.self).first!
                XCTAssertEqual(
                    DateFormatter.fullDateFormatter.string(from: latest.createdAt),
                    DateFormatter.fullDateFormatter.string(from: expectedCreatedAt)
                )
            case .failure:
                XCTFail("Should not fall into this case!")
            }
        }
    }
}

private extension WeatherForecastInteractorTests {
    func createTestData(createdAt: Date) throws {
        let forecastData = getDailyForecastModels()
        let weatherForecast = getWeatherForecastPersistence(createdAt: createdAt,
                                                            forecastData: forecastData)
        try realmStorage.save(object: weatherForecast)
    }
    
    func getWeatherForecastPersistence(createdAt: Date, forecastData: [DailyForecastModel]) -> WeatherForecastCachedModel {
        let weatherForecastPersistence = WeatherForecastCachedModel(cityId: Defaults.cityId,
                                                                    days: Defaults.days,
                                                                    tempUnit: Defaults.tempUnit,
                                                                    createAt: createdAt,
                                                                    forecastData: forecastData)
        return weatherForecastPersistence
    }
    
    func getDailyForecastModels() -> [DailyForecastModel] {
        let testBundle = Bundle(for: type(of: self))
        let fileName = "weather_forecast_mock"
        let listingJsonPath = testBundle.url(forResource: fileName, withExtension: "json")!
        let jsonData = try! Data(contentsOf: listingJsonPath)
        
        let data = try! JSONDecoder().decode(WeatherForecastResponse.self, from: jsonData)
        return data.list.map { DailyForecastModel(response: $0) }
    }
}

private class ConnectivityMock: Reachability {
    var isConnectedToInternet: Bool = false
    
    func reset() {
        isConnectedToInternet = false
    }
}
