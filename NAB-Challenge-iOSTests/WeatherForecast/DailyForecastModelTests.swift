//
//  DailyForecastModelTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 09/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

final class DailyForecastModelTests: XCTestCase {
    private lazy var dailyResponses = getDailyResponses()
    
    func test_dailyForecastModel_formatter() {
        let modelFromResponse = DailyForecastModel(response: dailyResponses[0])
        let model = DailyForecastModel(date: Date(timeIntervalSince1970: 1631160000),
                                       averageTemp: "27",
                                       pressure: "1009",
                                       humidity: "65",
                                       descn: "moderate rain")
        
        XCTAssertEqual(model, modelFromResponse)
    }
    
    func test_dailyForecastModel_withSameResponse() {
        let model1 = DailyForecastModel(response: dailyResponses[0])
        let model2 = DailyForecastModel(response: dailyResponses[0])
        
        XCTAssertEqual(model1, model2)
    }
    
    func test_dailyForecastModel_withDifferentResponse() {
        let model1 = DailyForecastModel(response: dailyResponses[0])
        let model2 = DailyForecastModel(response: dailyResponses[1])
        
        XCTAssertNotEqual(model1, model2)
    }
}

private extension DailyForecastModelTests {
    func getDailyResponses() -> [WeatherForecastResponseDaily] {
        let testBundle = Bundle(for: type(of: self))
        let fileName = "weather_forecast_mock"
        let listingJsonPath = testBundle.url(forResource: fileName, withExtension: "json")!
        let jsonData = try! Data(contentsOf: listingJsonPath)
        
        let data = try! JSONDecoder().decode(WeatherForecastResponse.self, from: jsonData)
        return data.list
    }
}
