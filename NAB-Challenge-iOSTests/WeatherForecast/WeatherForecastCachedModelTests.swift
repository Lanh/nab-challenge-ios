//
//  WeatherForecastCachedModelTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 09/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

private enum Defaults {
    static let createdAt = Date()
    static let cityId = 12345
    static let days = 7
    static let tempUnit = TempUnit.metric.rawValue
}

final class WeatherForecastCachedModelTests: XCTestCase {
    private lazy var dailyForecastModels = getDailyForecastModels()
    
    override func setUpWithError() throws {}
    
    override func tearDownWithError() throws {}
    
    func test_weatherForecastCachedModel_withSameData() {
        let firstModel = WeatherForecastCachedModel(cityId: Defaults.cityId,
                                                    days: Defaults.days,
                                                    tempUnit: Defaults.tempUnit,
                                                    createAt: Defaults.createdAt,
                                                    forecastData: dailyForecastModels)
        let secondModel = WeatherForecastCachedModel(cityId: Defaults.cityId,
                                                     days: Defaults.days,
                                                     tempUnit: Defaults.tempUnit,
                                                     createAt: Defaults.createdAt,
                                                     forecastData: dailyForecastModels)
        
        XCTAssertEqual(firstModel, secondModel)
    }
    
    func test_weatherForecastCachedModel_withSameForecastExceptDate() {
        let firstModel = WeatherForecastCachedModel(cityId: Defaults.cityId,
                                                    days: Defaults.days,
                                                    tempUnit: Defaults.tempUnit,
                                                    createAt: Defaults.createdAt,
                                                    forecastData: dailyForecastModels)
        let secondModel = WeatherForecastCachedModel(cityId: Defaults.cityId,
                                                     days: Defaults.days,
                                                     tempUnit: Defaults.tempUnit,
                                                     createAt: Defaults.createdAt.addingTimeInterval(1),
                                                     forecastData: dailyForecastModels)
        
        XCTAssertNotEqual(firstModel, secondModel)
    }
    
}

private extension WeatherForecastCachedModelTests {
    func getDailyForecastModels() -> [DailyForecastModel] {
        let testBundle = Bundle(for: type(of: self))
        let fileName = "weather_forecast_mock"
        let listingJsonPath = testBundle.url(forResource: fileName, withExtension: "json")!
        let jsonData = try! Data(contentsOf: listingJsonPath)
        
        let data = try! JSONDecoder().decode(WeatherForecastResponse.self, from: jsonData)
        return data.list.map { DailyForecastModel(response: $0) }
    }
}
