//
//  WeatherForecastPresenterTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 08/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

private enum Defaults {
    static let cityViewData = CityViewData(
        city: CityModel(id: 123456, name: "London", country: "GB"),
        isFromSearchHistory: false
    )
    static let weatherForecastList: [DailyForecastModel] = [
        DailyForecastModel(date: Date(),
                           averageTemp: "30",
                           pressure: "1000",
                           humidity: "50",
                           descn: "It's hot")
    ]
}

final class WeatherForecastPresenterTests: XCTestCase {
    private var sut: WeatherForecastPresenter!
    private let interactor = WeatherForecastInteractorMock()
    private let router = WeatherForecastRouterMock()
    private let view = WeatherForecastViewMock()
    
    override func setUpWithError() throws {
        sut = WeatherForecastPresenter(interactor: interactor, router: router)
    }
    
    override func tearDownWithError() throws {
        interactor.reset()
        router.reset()
        view.reset()
    }
    
    func test_didStart_withoutDataChange() {
        sut.view = view
        sut.didStart()
        XCTAssertEqual(router.embedCitySearchingWasCalled, true)
        XCTAssertEqual(view.displayLoadingStateWasCalled, false)
        XCTAssertEqual(view.updateSearchBarWasCalled, false)
    }
    
    func test_didStart_withDataChange() {
        sut.view = view
        router.hasDataChanged = true
        sut.didStart()
        XCTAssertEqual(router.embedCitySearchingWasCalled, true)
        XCTAssertEqual(view.displayLoadingStateWasCalled, true)
        XCTAssertEqual(view.updateSearchBarWasCalled, true)
    }
    
    func test_didStart_withDataChangeAndValidCached() {
        interactor.hasValidCachedWeatherForecast = true
        interactor.weatherForecastList = Defaults.weatherForecastList
        router.hasDataChanged = true
        sut.didStart()
        XCTAssertEqual(interactor.fetchWeatherForecastWasCalled, false)
    }
    
    func test_didStart_withDataChangeAndInvalidCachedAndNoResults() {
        sut.view = view
        interactor.hasValidCachedWeatherForecast = false
        interactor.weatherForecastList = []
        router.hasDataChanged = true
        sut.didStart()
        XCTAssertEqual(interactor.fetchWeatherForecastWasCalled, true)
        XCTAssertEqual(view.displayErrorStateWasCalled, true)
        XCTAssertEqual(view.reloadWasCalled, false)
    }
    
    func test_didStart_withDataChangeAndInvalidCachedAndHadResults() {
        sut.view = view
        interactor.hasValidCachedWeatherForecast = false
        interactor.weatherForecastList = Defaults.weatherForecastList
        router.hasDataChanged = true
        sut.didStart()
        XCTAssertEqual(interactor.fetchWeatherForecastWasCalled, true)
        XCTAssertEqual(view.displayErrorStateWasCalled, false)
        XCTAssertEqual(view.reloadWasCalled, true)
    }
}

private class WeatherForecastInteractorMock: WeatherForecastInteraction {
    var fetchWeatherForecastWasCalled = false
    var hasValidCachedWeatherForecast = false
    var weatherForecastList = [DailyForecastModel]()
    
    func fetchWeatherForecast(by cityId: Int, isMocked: Bool, completion: @escaping (Result<[DailyForecastModel], Error>) -> Void) {
        fetchWeatherForecastWasCalled = true
        if weatherForecastList.count > 0 {
            completion(.success(weatherForecastList))
        } else {
            completion(.failure(NetworkError.notFound))
        }
    }
    
    func hasValidCachedWeatherForecast(by cityId: Int) -> (Bool, [DailyForecastModel]) {
        return (hasValidCachedWeatherForecast, weatherForecastList)
    }
    
    func reset() {
        fetchWeatherForecastWasCalled = false
        hasValidCachedWeatherForecast = false
    }
}

private class WeatherForecastRouterMock: WeatherForecastRoutes {
    var embedCitySearchingWasCalled = false
    var hasDataChanged = false
    
    func embedCitySearching(_ onChangeDataAction: OnChangeDataAction?) {
        embedCitySearchingWasCalled = true
        if hasDataChanged,
           let onChangeDataAction = onChangeDataAction {
            onChangeDataAction(Defaults.cityViewData)
        }
    }
    
    func reset() {
        embedCitySearchingWasCalled = false
        hasDataChanged = false
    }
}

private class WeatherForecastViewMock: UIViewController, WeatherForecastViewProtocol {
    var displayLoadingStateWasCalled = false
    var displayErrorStateWasCalled = false
    var reloadWasCalled = false
    var updateSearchBarWasCalled = false
    
    func displayLoadingState() {
        displayLoadingStateWasCalled = true
    }
    
    func displayErrorState(errorMessage: String) {
        displayErrorStateWasCalled = true
    }
    
    func reload(with viewData: [DailyForecastViewData]) {
        reloadWasCalled = true
    }
    
    func updateSearchBar(with searchText: String) {
        updateSearchBarWasCalled = true
    }
    
    func reset() {
        displayLoadingStateWasCalled = false
        displayErrorStateWasCalled = false
        reloadWasCalled = false
        updateSearchBarWasCalled = false
    }
}
