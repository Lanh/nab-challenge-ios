//
//  CitySearchingPresenterTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 10/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

private enum Defaults {
    static let cityId = 1580578
    static let cityName = "Ho Chi Minh City"
    static let cityModel = CityModel(id: cityId,
                                     name: cityName,
                                     country: "VN")
}

final class CitySearchingPresenterTests: XCTestCase {
    private var sut: CitySearchingPresenter!
    private let interactor = CitySearchingInteractorMock()
    private let view = CityViewMock()
    
    override func setUpWithError() throws {
        sut = CitySearchingPresenter(interactor: interactor)
    }
    
    override func tearDownWithError() throws {
        interactor.reset()
        view.reset()
    }
    
    func test_startSearching() {
        sut.view = view
        sut.startSearching()
        XCTAssertEqual(interactor.getSearchHistoryWasCalled, true)
        XCTAssertEqual(view.reloadWasCalled, true)
    }
    
    func test_searching_withEmptyKeyword() {
        sut.view = view
        sut.startSearching()
        XCTAssertEqual(interactor.getSearchHistoryWasCalled, true)
        XCTAssertEqual(view.reloadWasCalled, true)
    }
    
    func test_searching_withValidCityName() {
        sut.view = view
        sut.searching(keyword: Defaults.cityName)
        XCTAssertEqual(interactor.getCitiesWasCalled, true)
        XCTAssertEqual(view.reloadWasCalled, true)
        XCTAssertEqual(view.displayNoCityFoundStateWasCalled, false)
    }
    
    func test_searching_withInvalidCityName() {
        sut.view = view
        sut.searching(keyword: "Not Existed City Name")
        XCTAssertEqual(interactor.getCitiesWasCalled, true)
        XCTAssertEqual(view.reloadWasCalled, false)
        XCTAssertEqual(view.displayNoCityFoundStateWasCalled, true)
    }
    
    func test_finishSearching() {
        sut.finishSearching(with: Defaults.cityId)
        XCTAssertEqual(interactor.saveSearchHistoryWasCalled, true)
    }
}

private class CitySearchingInteractorMock: CitySearchingInteraction {
    var getCitiesWasCalled = false
    var getSearchHistoryWasCalled = false
    var saveSearchHistoryWasCalled = false
    
    func getCities(keyword: String) -> [CityModel] {
        getCitiesWasCalled = true
        return Defaults.cityName.contains(keyword) ? [Defaults.cityModel] : []
    }
    
    func getSearchHistory() -> [CityModel] {
        getSearchHistoryWasCalled = true
        return [Defaults.cityModel]
    }
    
    func saveSearchHistory(cityId: Int) {
        saveSearchHistoryWasCalled = true
    }
    
    func reset() {
        getCitiesWasCalled = false
        getSearchHistoryWasCalled = false
        saveSearchHistoryWasCalled = false
    }
}

private class CityViewMock: UIViewController, CityViewProtocol {
    var onSelectCity: OnChangeDataAction?
    var displayNoCityFoundStateWasCalled = false
    var reloadWasCalled = false
    
    func displayNoCityFoundState() {
        displayNoCityFoundStateWasCalled = true
    }
    
    func reload(with cities: [CityViewData]) {
        reloadWasCalled = true
    }
    
    func reset() {
        displayNoCityFoundStateWasCalled = false
        reloadWasCalled = false
    }
}
