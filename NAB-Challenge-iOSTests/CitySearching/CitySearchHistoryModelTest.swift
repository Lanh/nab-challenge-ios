//
//  CitySearchHistoryModelTest.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 10/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

final class CitySearchHistoryModelTest: XCTestCase {
    override func setUpWithError() throws {}
    
    override func tearDownWithError() throws {}
    
    func test_citySearchHistoryModel_withTheSameData() {
        let cityId = 1580578
        let createdAt = Date()
        
        let firstModel = CitySearchHistoryModel(cityId: cityId,
                                                createdAt: createdAt)
        
        let secondModel = CitySearchHistoryModel(cityId: cityId,
                                                 createdAt: createdAt)
        
        XCTAssertEqual(firstModel, secondModel)
    }
    
    func test_citySearchHistoryModel_withDifferentData() {
        let createdAt = Date()
        
        let firstModel = CitySearchHistoryModel(cityId: 1580578,
                                                createdAt: createdAt)
        
        let secondModel = CitySearchHistoryModel(cityId: 894239,
                                                 createdAt: createdAt.addingTimeInterval(1))
        
        XCTAssertNotEqual(firstModel, secondModel)
    }
}
