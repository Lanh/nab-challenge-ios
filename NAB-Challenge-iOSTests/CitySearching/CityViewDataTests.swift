//
//  CityViewDataTest.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 10/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

private enum Defaults {
    static let firstCityModel = CityModel(id: 1580578,
                                          name: "Ho Chi Minh City",
                                          country: "VN")
    
    static let secondCityModel = CityModel(id: 894239,
                                           name: "Chegutu",
                                           country: "Zimbabwe")
}

final class CityViewDataTests: XCTestCase {
    override func setUpWithError() throws {}
    
    override func tearDownWithError() throws {}
    
    func test_cityViewData_withTheSameModel() {
        let firstViewData = CityViewData(city: Defaults.firstCityModel, isFromSearchHistory: true)
        let secondViewData = CityViewData(city: Defaults.firstCityModel, isFromSearchHistory: true)
        XCTAssertEqual(firstViewData, secondViewData)
    }
    
    func test_cityViewData_withTheSameModelAndDifferentIsFromSearchHistory() {
        let firstViewData = CityViewData(city: Defaults.firstCityModel, isFromSearchHistory: true)
        let secondViewData = CityViewData(city: Defaults.firstCityModel, isFromSearchHistory: false)
        XCTAssertNotEqual(firstViewData, secondViewData)
    }
    
    func test_cityViewData_withDifferentModel() {
        let firstViewData = CityViewData(city: Defaults.firstCityModel, isFromSearchHistory: true)
        let secondViewData = CityViewData(city: Defaults.secondCityModel, isFromSearchHistory: true)
        XCTAssertNotEqual(firstViewData, secondViewData)
    }
    
    func test_cityViewData_withDifferentModelAndDifferentIsFromSearchHistory() {
        let firstViewData = CityViewData(city: Defaults.firstCityModel, isFromSearchHistory: true)
        let secondViewData = CityViewData(city: Defaults.secondCityModel, isFromSearchHistory: false)
        XCTAssertNotEqual(firstViewData, secondViewData)
    }
}
