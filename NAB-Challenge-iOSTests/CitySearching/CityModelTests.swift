//
//  CityModelTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 10/09/2021.
//

import XCTest
@testable import NAB_Challenge_iOS

final class CityModelTests: XCTestCase {
    override func setUpWithError() throws {}
    
    override func tearDownWithError() throws {}
    
    func test_cityModel_withTheSameData() {
        let cityId = 1580578
        let cityName = "Ho Chi Minh City"
        let country = "VN"
        
        let firstCityModel = CityModel(id: cityId,
                                       name: cityName,
                                       country: country)
        
        let secondCityModel = CityModel(id: cityId,
                                        name: cityName,
                                        country: country)
        
        XCTAssertEqual(firstCityModel, secondCityModel)
    }
    
    func test_cityModel_withDifferentData() {
        let firstCityModel = CityModel(id: 1580578,
                                       name: "Ho Chi Minh City",
                                       country: "VN")
        
        let secondCityModel = CityModel(id: 894239,
                                        name: "Chegutu",
                                        country: "Zimbabwe")
        
        XCTAssertNotEqual(firstCityModel, secondCityModel)
    }
}
