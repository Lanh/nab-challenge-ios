//
//  CitySearchingInteractorTests.swift
//  NAB-Challenge-iOSTests
//
//  Created by Lanh Vo on 10/09/2021.
//

import XCTest
import RealmSwift
@testable import NAB_Challenge_iOS

private enum Defaults {
    static let cityId = 894239
    static let cityName = "Chegutu"
    static let countryName = "Zimbabwe"
    static let cityModel = CityModel(id: cityId,
                                     name: cityName,
                                     country: countryName)
}

final class CitySearchingInteractorTests: XCTestCase {
    private var sut: CitySearchingInteractor!
    
    private lazy var realmStorage: RealmStorageContext = {
        let configuration = Realm.Configuration(inMemoryIdentifier: "CitySearchingInteractorTests")
        return RealmStorageContext(configuration: configuration)
    }()
    
    override func setUpWithError() throws {
        sut = CitySearchingInteractor(storage: realmStorage)
    }
    
    override func tearDownWithError() throws {
        try realmStorage.reset()
    }
    
    func test_getCities_withValidCityName() throws {
        // Given
        let expectedCityModels = [Defaults.cityModel]
        try createTestData()
        
        // When
        let cityModels = sut.getCities(keyword: Defaults.cityName)
        
        // Then
        XCTAssertEqual(cityModels, expectedCityModels)
    }
    
    func test_getCities_withInvalidCityName() throws {
        // Given
        let expectedCityModels = [CityModel]()
        try createTestData()
        
        // When
        let cityModels = sut.getCities(keyword: "Not Existed City Name")
        
        // Then
        XCTAssertEqual(cityModels, expectedCityModels)
    }
    
    func test_getCities_withValidCountryName() throws {
        // Given
        let expectedCityModel = Defaults.cityModel
        try createTestData()
        
        // When
        let cityModels = sut.getCities(keyword: Defaults.countryName)
        
        // Then
        XCTAssertTrue(cityModels.contains(expectedCityModel))
        XCTAssertTrue(cityModels.count > 1)
    }
    
    func test_getCities_withInvalidCountryName() throws {
        // Given
        let expectedCityModels = [CityModel]()
        try createTestData()
        
        // When
        let cityModels = sut.getCities(keyword: "Not Existed Country Name")
        
        // Then
        XCTAssertEqual(cityModels, expectedCityModels)
    }
    
    func test_getSearchHistory_withNoSavedData() {
        // Given
        let expectedCityModels = [CityModel]()
        
        // When
        let cityModels = sut.getSearchHistory()
        
        // Then
        XCTAssertEqual(cityModels, expectedCityModels)
    }
    
    func test_getSearchHistory_withSavedData() throws {
        // Given
        let expectedCityModels = [Defaults.cityModel]
        try createTestData()
        try createSearchHistory(createdAt: Date())
        
        // When
        let cityModels = sut.getSearchHistory()
        
        // Then
        XCTAssertEqual(cityModels, expectedCityModels)
    }
    
    func test_saveSearchHistory_withCreateNew() {
        // Given
        let expectedSearchHistories = [CitySearchHistoryModel(
            cityId: Defaults.cityId,
            createdAt: Date()
        )]
        
        // When
        sut.saveSearchHistory(cityId: Defaults.cityId)
        
        // Then
        let searchHistories = realmStorage.fetch(CitySearchHistoryModel.self)
        XCTAssertEqual(searchHistories, expectedSearchHistories)
    }
    
    func test_saveSearchHistory_withUpdate() throws {
        // Given
        let expectedSearchHistories = [CitySearchHistoryModel(
            cityId: Defaults.cityId,
            createdAt: Date()
        )]
        try createSearchHistory(createdAt: Date().addingTimeInterval(-1))
        
        // When
        sut.saveSearchHistory(cityId: Defaults.cityId)
        
        // Then
        let searchHistories = realmStorage.fetch(CitySearchHistoryModel.self)
        XCTAssertEqual(searchHistories, expectedSearchHistories)
    }
}

private extension CitySearchingInteractorTests {
    func createTestData() throws {
        let testBundle = Bundle(for: type(of: self))
        let listingJsonPath = testBundle.url(forResource: "city_list_mock", withExtension: "json")!
        let cities = try JSONDecoder().decode([CityModel].self, from: Data(contentsOf: listingJsonPath))
        try realmStorage.save(objects: cities)
    }
    
    func createSearchHistory(createdAt: Date) throws {
        let searchHistory = CitySearchHistoryModel(
            cityId: Defaults.cityId,
            createdAt: createdAt
        )
        try realmStorage.save(object: searchHistory)
    }
}
