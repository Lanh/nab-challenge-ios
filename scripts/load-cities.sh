# Make sure the required json file exists
check_file_exists()
{
    DIRECTORY=$1

    echo "Looking for ${FILE_NAME} in ${DIRECTORY}"
    if [ ! -f $DIRECTORY ]
    then
        echo "File not found for $DIRECTORY. Please ensure it's in the proper directory."
        exit 1
    fi
}

FILE_NAME=world-cities.json
FILE_DIR=${PROJECT_DIR}/NAB-Challenge-iOS/Shared/Helpers/${FILE_NAME}
check_file_exists ${FILE_DIR}

# Get a reference to the destination location for the json file
FILE_DESTINATION=${BUILT_PRODUCTS_DIR}/${PRODUCT_NAME}.app
echo "Will copy ${FILE_DIR} to final destination: ${FILE_DESTINATION}"

# Copy the json file
cp "${FILE_DIR}" "${FILE_DESTINATION}"