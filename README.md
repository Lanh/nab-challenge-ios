# NAB - iOS Challenge

## Installation
- Clone the [project](https://bitbucket.org/Lanh/nab-challenge-ios/src/main/).
- Install the [CocoaPods](https://guides.cocoapods.org/using/getting-started.html). Recommended version: ```1.10.1```.
- Open Terminal, navigate to the project folder and Run ```pod install```.

## Supported environments and devices
- iOS 12.0 and above.
- iPhone (Portrait).
- iPad (Portrait and Landscape).

## Checklist
- Programming language: ```Swift 5```.
- Architectural design: VIPER.
- UI should be looks like in attachment.
- Unit Tests.
- Exception handling.
- Caching handling.
- Accessibility for Disability Supports: VoiceOver and Scaling Text.
- Entity relationship diagram.
- Readme file.

## Features
- ##### Weather Forecast: 
    - Display the weather forecast of the selected city.
        - ![Weather Forecast](images/weather_forecast.PNG)

- ##### City Searching: 
    - Display a list of possible cities (from the local list of city) related to the input keywords (city name or country name). 
        - ![City Searching](images/city_searching.PNG)
    - Display a list of history cities from previous searches.
        - ![City Searched History](images/city_searched_history.PNG)

## Exception handling
 - Display error message when no city found or no internet connection, or server errors.
    - ![Loading](images/waether_forecast_loading.PNG)
    - ![No Internet](images/weather_forecast_error.PNG)
    - ![No Results](images/city_searching_no_results.PNG)

## Structure of the project
- ![Structure](images/project_structure.png)

## Unit testing
- ![Structure](images/unit_tests.png)

## Entity relationship diagram for the database 
- ![Structure](images/erd_cached.png)

## The app flow
- ![Structure](images/flow.png)

## The city list data
- To improve the UX and reduce the server hits, I decided to use the local [list of the city](https://datahub.io/core/world-cities/r/world-cities.json) for searching. In detail, the JSON file will be read and saved into the local database at launch time (only one per install).
- Later on, the selected city ID will be used to fetch the weather forecast data from the [API](https://openweathermap.org/forecast16). This approach will avoid the ambiguous data when using the city name as [the API provider recommended](https://openweathermap.org/forecast16).

## Caching mechanism
- To reduce the number of requests to the server, the caching mechanism has been applied. The weather forecast data will be saved into the local database with the search criteria (city ID, days of forecast, temperature unit). If the cached data is still valid (5 minutes lifetime), it will be used to display on UI. Otherwise, a request to the server will be triggered.

## 3rd-party libraries
* ##### Networking
    * Alamofire - 5.4.3 - https://github.com/Alamofire/Alamofire
    * Moya - 15.0.0 - https://github.com/Moya/Moya
* ##### Layouting
    * EasyPeasy - 1.10.0 - https://github.com/nakiostudio/EasyPeasy
* ##### Database storage
    * Realm - 10.14.0 - https://github.com/realm/realm-cocoa

## Possible improvements
- ```git-secret``` is a good place to store the APIKey. However, this challenge required the git repository to be public, so this approach will be saved for later. 
- Providing the option to change the days of forecast and temperature units.
- Dark mode supporting and UI testing will be nice features to have.

## Author
- Lanh Vo - votulanh@gmail.com
